const express = require("express");
const mongoose = require("mongoose");
const app = express();

// Connexion à MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/gestion-films");

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Erreur de connexion à MongoDB :"));
db.once("open", () => {
  console.log("Connexion à MongoDB établie avec succès");
});

// Définition du modèle Film
const Film = mongoose.model("Film", {
  titre: String,
  réalisateur: String,
  année: Number,
});

// Route pour obtenir tous les films
app.get("/films", async (req, res) => {
  try {
    const films = await Film.find();
    res.json(films);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//ajouter un film
app.post("/films", async (req, res) => {
  const { titre, réalisateur, année } = req.body;
  try {
    const film = new Film({ titre, réalisateur, année });
    await film.save();
    res.status(201).json(film);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

app.get("/films/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const film = await Film.findById(id);
    if (!film) {
      return res.status(404).json({ message: "Film non trouvé" });
    }
    res.json(film);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//modifier
app.put("/films/:id", async (req, res) => {
  const id = req.params.id;
  const { titre, réalisateur, année } = req.body;
  try {
    const film = await Film.findByIdAndUpdate(
      id,
      { titre, réalisateur, année },
      { new: true }
    );
    if (!film) {
      return res.status(404).json({ message: "Film non trouvé" });
    }
    res.json(film);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//supprimer
app.delete("/films/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const film = await Film.findByIdAndDelete(id);
    if (!film) {
      return res.status(404).json({ message: "Film non trouvé" });
    }
    res.json({ message: "Film supprimé avec succès" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Serveur démarré sur le port ${PORT}`);
});
